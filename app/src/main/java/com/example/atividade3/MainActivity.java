package com.example.atividade3;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.os.Bundle;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private EditText editTextX;
    private EditText editTextY;
    private EditText editTextZ;
    private SensorManager sensorManager;
    private Sensor acelerometro;
    private Float ultimoX;
    private Float ultimoY;
    private Float ultimoZ;
    private final static float SENSIBILIDADE = 30;
    private boolean ligada = false;
    private CameraManager camManager;
    private String cameraId = null;
    private long tempo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextX = findViewById(R.id.editTextX);
        editTextY = findViewById(R.id.editTextY);
        editTextZ = findViewById(R.id.editTextZ);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        acelerometro = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        registraSensor();
    }

    private void registraSensor() {
        sensorManager.registerListener(this, acelerometro, SensorManager.SENSOR_DELAY_NORMAL);
    }

    private void desRegistraSensor() {
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        float x = (float) (Math.round(sensorEvent.values[0] * Math.pow(10, 2)) / Math.pow(10, 2));
        float y = (float) (Math.round(sensorEvent.values[1] * Math.pow(10, 2)) / Math.pow(10, 2));
        float z = (float) (Math.round(sensorEvent.values[2] * Math.pow(10, 2)) / Math.pow(10, 2));

        editTextX.setText("X: " + Float.toString(x));
        editTextY.setText("Y: " + Float.toString(y));
        editTextZ.setText("Z: " + Float.toString(z));

        if (ultimoX != null && (Math.abs(x - ultimoX) > SENSIBILIDADE || Math.abs(y - ultimoY) > SENSIBILIDADE || Math.abs(z - ultimoZ) > SENSIBILIDADE))
        {
            if (System.currentTimeMillis() - tempo > 1000) {
                if (ligada) {
                    ligada = false;
                    desligaLanterna();
                } else {
                    ligada = true;
                    ligaLanterna();
                }
            }
        }

        ultimoX = x;
        ultimoY = y;
        ultimoZ = z;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    protected void onPause() {
        desRegistraSensor();
        super.onPause();
    }

    @Override
    protected void onResume() {
        registraSensor();
        super.onResume();
    }

    private void ligaLanterna() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            camManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
            try {
                cameraId = camManager.getCameraIdList()[0];
                camManager.setTorchMode(cameraId, true);   //Turn ON
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
            tempo = System.currentTimeMillis();
        }
        startActivity(new Intent(this, PainelActivity.class));
    }

    private void desligaLanterna() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try {
                camManager.setTorchMode(cameraId, false);
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
            tempo = System.currentTimeMillis();
        }
    }
}