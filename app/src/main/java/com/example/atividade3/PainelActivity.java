package com.example.atividade3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class PainelActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_painel);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}